% Image Annotation
% 
% Description:
%   This program tags object instances in images. In detail, the program uses 
%   a MATLAB GUI interface to annotate objects. This program is useful for 
%   introducing annotations in datasets (e.g ground truth). 
%
%   This code is only for research and educational purposes.
%
% Steps:
%   Steps to exucute the program:
%     1. Run the prg_setup.m file to configure the program paths and compile the mex 
%        files.
%     2. Run the program.m file to annotate objects in images. 
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014

%% program
function varargout = program(varargin)
% PROGRAM MATLAB code for program.fig
%      PROGRAM, by itself, creates a new PROGRAM or raises the existing
%      singleton*.
%
%      H = PROGRAM returns the handle to a new PROGRAM or the handle to
%      the existing singleton*.
%
%      PROGRAM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROGRAM.M with the given input arguments.
%
%      PROGRAM('Property','Value',...) creates a new PROGRAM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before program_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to program_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help program

% Last Modified by GUIDE v2.5 16-Mar-2012 15:08:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @program_OpeningFcn, ...
                   'gui_OutputFcn',  @program_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

%% --- Executes just before program is made visible.
function program_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to program (see VARARGIN)

% Choose default command line output for program
handles.output = hObject;

% initialize
handles = fun_initialize(hObject, eventdata, handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes program wait for user response (see UIRESUME)
% uiwait(handles.guiProgram);
end

%% --- Outputs from this function are returned to the command line.
function varargout = program_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

%% --- Executes on button press in butNext.
function butNext_Callback(hObject, eventdata, handles)
% hObject    handle to butNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% check: no more images
if (handles.iterImage == handles.numImages)
    msgbox('no more images in the current path','Warning');
    return;
end

% output path
outPath = 'annotations';
               
% creating output directory
[success,~,~] = mkdir(sprintf('%s/%s',handles.path,outPath));
if (success==0), error('error creating the output directory'); end

% save .mat Image
data = handles.matImage;
save(sprintf('%s/%s/%s.mat',handles.path,outPath,handles.imageName),'data');

% increment the current image in the sequence (image directory)
iterImage = handles.iterImage + 1;

% image name
imgName = handles.imgFiles(iterImage).name;

% read image
cImg = imread([handles.path, imgName]);

% variables
handles.iterImage = iterImage;
handles.imageName = imgName;
handles.image = cImg;

% set
set(handles.textImage,'string',imgName);
set(handles.butSave,'enable','off');
set(handles.popupmenuObjects,'enable','off');
set(handles.butUndo,'enable','off');
set(handles.butCrop,'enable','on');

% reset variables
handles = fun_reset_variables(handles);

% save
guidata(hObject,handles)

% show image
fun_show_image(hObject, eventdata, handles);
end

%% --- Executes on button press in butCrop.
function butCrop_Callback(hObject, eventdata, handles)
% hObject    handle to butCrop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% bounding box
[patch,bbox] = fun_image_crop(handles.image,handles.bbox);

% random color
rndColor = zeros(1,3);
while (sum(rndColor,2) < 0.6); rndColor = rand(1,3); end

% image
axes(handles.axesImage)

% variables
handles.bbox = bbox;
handles.drawBBox = rectangle('position',[bbox(1:2),bbox(3:4)-bbox(1:2)],'edgecolor',rndColor,'linewidth',3);
handles.drawLabel = text(bbox(1,1),bbox(1,2),'object','color',rndColor);

% set
set(handles.butUndo,'enable','on')
set(handles.butNext,'enable','off')
set(handles.butCrop,'enable','off')
set(handles.popupmenuObjects,'enable','on');

% save
guidata(hObject,handles)
end

%% --- Executes on button press in butUndo.
function butUndo_Callback(hObject, eventdata, handles)
% hObject    handle to butUndo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% variables
handles.label = ' ';

% clear current bounding box
reset(handles.drawBBox);
reset(handles.drawLabel);

% set
set(handles.butSave,'enable','off');
set(handles.butUndo,'enable','off');
set(handles.popupmenuObjects,'enable','off');
set(handles.butNext,'enable','on');
set(handles.butCrop,'enable','on');

% save
guidata(hObject,handles)
end

%% --- Executes on button press in butSave.
function butSave_Callback(hObject, eventdata, handles)
% hObject    handle to butSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% num. objects
numObjects = handles.matImage.numObjects + 1;

% mat. image
matImage = handles.matImage;

% mat file
matImage.name = handles.imageName;
matImage.object{numObjects}.name  = handles.label;
matImage.object{numObjects}.bbox  = handles.bbox;
matImage.numObjects = numObjects;

% variables
handles.matImage = matImage;

% set
set(handles.butSave,'enable','off')
set(handles.butNext,'enable','on')
set(handles.butCrop,'enable','on')
set(handles.butUndo,'enable','off')

% save
guidata(hObject,handles)
end

%% --- Executes on selection change in popupmenuObjects.
function popupmenuObjects_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuObjects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuObjects contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuObjects

% obejct selected
cObject  = get(handles.popupmenuObjects,'value');

% bounding box
bbox = handles.bbox;

% object label
objectText = get(handles.popupmenuObjects,'string');
label = objectText(cObject,:);

% reset previous label
reset(handles.drawLabel); 

% image
axes(handles.axesImage)

% variables
handles.label = label;
handles.drawLabel = text(bbox(1,1),bbox(1,2),label,'color',[0.1,0.1,0.1]);

% set
set(handles.butSave,'enable','on');
set(handles.popupmenuObjects,'enable','off');

% save
guidata(hObject,handles)
end

%% --- Executes during object creation, after setting all properties.
function popupmenuObjects_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuObjects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% menu: settings
function menuSettings_Callback(hObject, eventdata, handles)
% hObject    handle to menuSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

%% menu: open image
function menuOpenImage_Callback(hObject, eventdata, handles)
% hObject    handle to menuOpenImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% image path
[fileName, filePath] = uigetfile('*.*','Open image');
if (ischar(fileName) == 0 && ischar(filePath) == 0); return; end

% image files in this path
imgFiles = dir(filePath);
    
% num images
numImages = size(imgFiles,1);

% indexing selected image
for iterImage = 1:numImages
    if (strcmp(fileName,imgFiles(iterImage).name)==1)
        imgName = fileName;
        imgIter = iterImage;
    end
end

% read image
cImg = imread([filePath, imgName]);

% variables
handles.image = cImg; 
handles.imageName = imgName;
handles.iterImage = imgIter;
handles.path = filePath;
handles.imgFiles = imgFiles;
handles.numImages = numImages;

% set
set(handles.butNext,'enable','on')
set(handles.butCrop,'enable','on')
set(handles.popupmenuObjects,'enable','off')

% saving values
guidata(hObject,handles)

% show image
fun_show_image(hObject, eventdata, handles);
end

%% menu: objects
function menuObjects_Callback(hObject, eventdata, handles)
% hObject    handle to menuObjects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% initialize
handles = fun_initialize(hObject, eventdata, handles);

% input object names
inputObjects = char(inputdlg('Enter object names in order (separated by ,)'));
objectNames = strread(inputObjects,'%s','delimiter',',');

% number of objects to tag
numObjects = size(objectNames,1);
objectNames = char(objectNames);

% variables
handles.numObjects = numObjects;

% set
set(handles.menuOpenImage,'enable','on');
set(handles.popupmenuObjects,'string',objectNames)

% save
guidata(hObject,handles)
end

%% show image
function fun_show_image(hObject, eventdata, handles)

% set
set(handles.textImage,'string',handles.imageName);
set(handles.axesImage,'visible','on');
axes(handles.axesImage), imshow(handles.image), axis off;

% save
guidata(hObject,handles)
end

%% .mat image
function outVar = fun_matImage()

% properties
matImage.name = '';
matImage.object = {};
matImage.numObjects = 0;

% output
outVar = matImage;
end

%% initialize
function outVar = fun_initialize(hObject, eventdata, handles)

% set
set(handles.axesImage,'visible','off');
set(handles.butNext,'enable','off');
set(handles.butCrop,'enable','off');
set(handles.butSave,'enable','off');
set(handles.butUndo,'enable','off');
set(handles.popupmenuObjects,'enable','off');
set(handles.menuObjects,'enable','on');
set(handles.menuOpenImage,'enable','off');

% variables
handles.bbox = [10,10,100,100];
handles.label = ' ';
handles.numObjects = 0;
handles.path = '.';
handles.imgFiles = [];
handles.numImages = 0;
handles.iterImage = 0;
handles.imageName = ' ';
handles.image = [];
handles.matImage = fun_matImage();
handles.drawBBox = [];
handles.drawLabel= [];

% output
outVar = handles;
end

%% reset variables
function outVar = fun_reset_variables(handles)

% variables
handles.label = ' ';
handles.numObjects = 0;
handles.matImage = fun_matImage();
handles.drawBBox = [];
handles.drawLabel= [];

% output
outVar = handles;
end

