%% image cropping program
function varargout = fun_image_crop(varargin)
%% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @fun_image_crop_OpeningFcn, ...
                   'gui_OutputFcn',  @fun_image_crop_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

%% image_crop_OpeningFcn
function fun_image_crop_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to myCroppingImage (see VARARGIN)

% Choose default command line output for myCroppingImage
handles.output = hObject;
tmp = {'img','bbox'};

% input structure
st = cell2struct(varargin,tmp,2);

% input image
img = getfield(st,'img');

% initial bounding box
handles.bbox = getfield(st,'bbox');

% load image
handles.image = img;

% Update handles structure
guidata(hObject, handles);

% application
fun_show_images(hObject, eventdata, handles);

% UIWAIT makes myCroppingImage wait for user response (see UIRESUME)
uiwait(handles.figure1);
end

%% Outputs from this function are returned to the command line.
function varargout = fun_image_crop_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.patch;
varargout{2} = handles.bbox;
delete(hObject);
end

%% Executes on button press in buttomOk.
function buttomOk_Callback(hObject, eventdata, handles)
% hObject    handle to buttomOk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get rectangle position (bbox)
rec = iptgetapi(handles.rectangle);

% bounding box
bbox = rec.getPosition();

% format
bbox = [bbox(1:2),bbox(1:2)+bbox(3:4)];

% get bounding box
handles.bbox = bbox;

% image patch
handles.patch = imcrop(handles.image,handles.bbox);

% Update handles structure
guidata(hObject, handles);

% application
fun_show_images(hObject, eventdata, handles);
end

%% Executes on button press in buttomExit.
function buttomExit_Callback(hObject, eventdata, handles)
% hObject    handle to buttomExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);
end

%% show image
function fun_show_images(hObject, eventdata, handles)

% show image
axes(handles.axesImage),imagesc(handles.image),colormap('gray'), axis off;

% bounding box
bbox = handles.bbox;

% format
bbox = [bbox(1:2), bbox(3:4)-bbox(1:2)];
handles.rectangle = imrect(gca,bbox);

% fixes aspect ratio
%setFixedAspectRatioMode(handles.rectangle,'true');

% image patch
handles.patch = imcrop(handles.image,bbox);

% show cropped image
axes(handles.axesCroppedImage),imagesc(handles.patch),colormap('gray'), axis off;

% GUI . coordinates
set(handles.text_coordinates,'string',sprintf('x %.1f y %.1f w %.1f h %.1f',bbox(1,:)));

% Update handles structure
guidata(hObject, handles);
end
