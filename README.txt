 Image Annotation
 
 Description:
   This program tags object instances in images. In detail, the program uses 
   a MATLAB GUI interface to annotate objects. This program is useful for 
   introducing annotations in datasets (e.g ground truth). 

   This code is only for research and educational purposes.

 Steps:
   Steps to exucute the program:
     1. Run the prg_setup.m file to configure the program paths and compile the mex 
        files.
     2. Run the program.m file to annotate objects in images. 

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2014

